#include "Core/Level/SQChunkManager.h"

#include "Core/Level/SQChunk.h"
#include "Core/Gameplay/SQPlayerController.h"

USQChunkManager::USQChunkManager()
{
	PrimaryComponentTick.bCanEverTick = true;

}

void USQChunkManager::BeginPlay()
{
	Super::BeginPlay();

	SpawnChunks();
	
}

void USQChunkManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

void USQChunkManager::SpawnChunks()
{
	for (int x = -1; x <= 1; ++x)
	{
		for (int y = -1; y <= 1; ++y)
		{
			for (int z = -1; z <= 1; ++z)
			{
				const FIntVector ChunkPos = FIntVector(x,y,z);
				FTransform ChunkTransform = FTransform();
				ChunkTransform.SetLocation(FVector(x,y,z) * 100 * 20);
				
				ASQChunk* Chunk = GetWorld()->SpawnActorDeferred<ASQChunk>(
					ASQChunk::StaticClass(),
					ChunkTransform,
					PlayerController,
					nullptr,
					ESpawnActorCollisionHandlingMethod::AlwaysSpawn);

				Chunk->ChunkPos = ChunkPos;
				
				Chunk->FinishSpawning(ChunkTransform);
				LoadedChunks.Add(ChunkPos,Chunk);
			}
		}
	}
}

