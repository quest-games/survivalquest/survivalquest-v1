﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/Level/SQVoxel.h"

int USQVoxel::GetIndex3D(int x, int y, int z, FIntVector ChunkSize)
{
	return z * ChunkSize.X * ChunkSize.Y + y * ChunkSize.X + x; 
}