#include "Core/Level/SQChunk.h"

#include "Core/Level/SQVoxel.h"

ASQChunk::ASQChunk()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	bOnlyRelevantToOwner = true;

	Mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("Mesh"));
	Mesh->bUseAsyncCooking = true;
}

void ASQChunk::BeginPlay()
{
	Super::BeginPlay();
	
}

void ASQChunk::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASQChunk::GenerateMap()
{
	NoiseMap.SetNum(
		(ChunkSize.X + 1) * (ChunkSize.Y + 1) * (ChunkSize.Z + 1)
	);

	int i = 0;
	for (int X = 0; X < ChunkSize.X + 1; ++X)
	{
		for (int Y = 0; Y < ChunkSize.Y + 1; ++Y)
		{
			for (int Z = 0; Z < ChunkSize.Z + 1; ++Z)
			{
				NoiseMap[i] = Z < 30 ? 1.0f : 0.0f;
				i++;
			}
		}
	}
}

void ASQChunk::ComputeChunk()
{	
	Vertices.Empty();
	Triangles.Empty();
	UVs.Empty();
	
	Colors.Empty();
	Normals.Empty();
	Tangents.Empty();

	if (SurfaceLevel > 0.0f)
	{
		TriangleOrder[0] = 0;
		TriangleOrder[1] = 1;
		TriangleOrder[2] = 2;
	}
	else
	{
		TriangleOrder[0] = 2;
		TriangleOrder[1] = 1;
		TriangleOrder[2] = 0;
	}

	float Cube[8];
	
	for (int X = 0; X < ChunkSize.X; ++X)
	{
		for (int Y = 0; Y < ChunkSize.Y; ++Y)
		{
			for (int Z = 0; Z < ChunkSize.Z; ++Z)
			{
				for (int i = 0; i < 8; ++i)
				{
					Cube[i] = NoiseMap[
						USQVoxel::GetIndex3D(
							X + VertexOffset[i][0],
							Y + VertexOffset[i][1],
							Z + VertexOffset[i][2],
							ChunkSize + FIntVector(1)
							)
						];
				}

				March(X,Y,Z, Cube);
			}
		}
	}

	RenderChunk();
}

void ASQChunk::March(int X, int Y, int Z, const float Cube[8])
{
	int VertexMask = 0;
	FVector EdgeVertex[12];

	for (int i = 0; i < 8; ++i)
	{
		if (Cube[i] <= SurfaceLevel) VertexMask |= 1 << i;
	}

	const int EdgeMask = CubeEdgeFlags[VertexMask];
	
	if (EdgeMask == 0) return;
		
	for (int i = 0; i < 12; ++i)
	{
		if ((EdgeMask & 1 << i) != 0)
		{
			const float Offset = GetInterpolationOffset(Cube[EdgeConnection[i][0]], Cube[EdgeConnection[i][1]]);

			EdgeVertex[i].X = X + (VertexOffset[EdgeConnection[i][0]][0] + Offset * EdgeDirection[i][0]);
			EdgeVertex[i].Y = Y + (VertexOffset[EdgeConnection[i][0]][1] + Offset * EdgeDirection[i][1]);
			EdgeVertex[i].Z = Z + (VertexOffset[EdgeConnection[i][0]][2] + Offset * EdgeDirection[i][2]);
		}
	}

	for (int i = 0; i < 5; ++i)
	{
		if (TriangleConnectionTable[VertexMask][3 * i] < 0) break;

		FVector V1 = EdgeVertex[TriangleConnectionTable[VertexMask][3 * i]] * 100;
		FVector V2 = EdgeVertex[TriangleConnectionTable[VertexMask][3 * i + 1]] * 100;
		FVector V3 = EdgeVertex[TriangleConnectionTable[VertexMask][3 * i + 2]] * 100;

		FVector Normal = FVector::CrossProduct(V3 - V1,V2 - V1);
		FColor Color = FColor::MakeRandomColor();
		
		Normal.Normalize();

		Vertices.Append({V1, V2, V3});
		
		Triangles.Append({
			VertexCount + TriangleOrder[0],
			VertexCount + TriangleOrder[1],
			VertexCount + TriangleOrder[2]
		});

		Normals.Append({
			Normal,
			Normal,
			Normal
		});

		Colors.Append({
			Color,
			Color,
			Color
		});

		VertexCount += 3;
	}
}

float ASQChunk::GetInterpolationOffset(float V1, float V2) const
{
	const float Delta = V2 - V1;
	return Delta == 0.0f ? SurfaceLevel : (SurfaceLevel - V1) / Delta;
}

void ASQChunk::RenderChunk()
{
	if (Vertices.Num() > 0 && Triangles.Num() > 0)
		Mesh->CreateMeshSection(0,Vertices,Triangles,Normals,UVs,Colors,Tangents,true);
}

