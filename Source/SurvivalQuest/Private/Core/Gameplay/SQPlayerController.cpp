#include "Core/Gameplay/SQPlayerController.h"

#include "Core/Level/SQChunkManager.h"

ASQPlayerController::ASQPlayerController()
{
	ChunkManager = CreateDefaultSubobject<USQChunkManager>(TEXT("ChunkManger"));
}

void ASQPlayerController::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	ChunkManager->PlayerController = this;	
}

void ASQPlayerController::BeginPlay()
{
	Super::BeginPlay();
}
