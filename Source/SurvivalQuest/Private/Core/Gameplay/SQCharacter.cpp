#include "Core/Gameplay/SQCharacter.h"

ASQCharacter::ASQCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

}

void ASQCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

void ASQCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASQCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

