// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SQCharacter.generated.h"

UCLASS()
class SURVIVALQUEST_API ASQCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ASQCharacter();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

};
