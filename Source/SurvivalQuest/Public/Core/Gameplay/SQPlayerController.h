#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "SQPlayerController.generated.h"

class USQChunkManager;

UCLASS()
class SURVIVALQUEST_API ASQPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	ASQPlayerController();

	virtual void OnConstruction(const FTransform& Transform) override;

	virtual void BeginPlay() override;
	
	UPROPERTY()
	USQChunkManager* ChunkManager;
};
