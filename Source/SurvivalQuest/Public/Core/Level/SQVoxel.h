﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "SQVoxel.generated.h"

/**
 * 
 */
UCLASS()
class SURVIVALQUEST_API USQVoxel : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:	
	static int GetIndex3D(int x, int y, int z, FIntVector ChunkSize);
};
