#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SQChunkManager.generated.h"


class ASQPlayerController;
class ASQChunk;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SURVIVALQUEST_API USQChunkManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	USQChunkManager();

protected:
	virtual void BeginPlay() override;

public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY()
	ASQPlayerController* PlayerController;

	void SpawnChunks();

	UPROPERTY()
	TMap<FIntVector,ASQChunk*> LoadedChunks;
};
