#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SQWorldManager.generated.h"

UCLASS()
class SURVIVALQUEST_API ASQWorldManager : public AActor
{
	GENERATED_BODY()
	
public:
	ASQWorldManager();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

};
